const synth=window.speechSynthesis; //Speech Synthesis client

//DOM Elements
const textForm = document.querySelector("form");
const textInput = document.querySelector("#text-input");
const voiceSelect = document.querySelector("#voice-select");
const rate = document.querySelector("#rate");
const rateValue = document.querySelector("#rate-value");
const pitch = document.querySelector("#pitch");
const pitchValue = document.querySelector("#pitch-value");
const body = document.querySelector("body");

//Fetching voices using speechSynthesis API
let voices = [];
//Appending voices to the list
const getVoices = async () => {
    voices=synth.getVoices(); //Async call. May take time.
    console.log(voices);
    voices.forEach(voice => {
        const option = document.createElement("option");
        option.textContent = voice.name+" ("+voice.lang+")"; //Displaying name (language)
        option.setAttribute("data-lang",voice.lang);
        option.setAttribute("data-name",voice.name);
        voiceSelect.appendChild(option);
    });
};
//Async delay causes default as undefined.
//Taking care of it by appending get voices after voices have been fetched
getVoices();
if(synth.onvoiceschanged != undefined){
    synth.onvoiceschanged = getVoices;
}

const speak = () => {
    //Check if something is already being played
    if(synth.speaking){
        console.error("Speech is in progress");
        return;
    }
    //Giving user what he wants except for when he wants nothing
    if(textInput.value != ""){
        const speakTxt = new SpeechSynthesisUtterance(textInput.value);
        speakTxt.onend = e => {
            console.log("Speech is finished")
        }

        speakTxt.onerror = e => {
            consloe.error("Oops! Someting went wrong....");
        }
        //Passing the voice
        const seletVoice = voiceSelect.selectedOption[0].getAttribute("data-name");
        voices.forEach(voice => {
            if(voice.name === seletVoice){
                speakTxt.voice=voice;
            }
        });
        //Checking for rate and pitch
        speakTxt.rate = rate.value;
        speakTxt.pitch = pitch.value;
        //The app opens it's mouth
        synth.speak(speakTxt);
    }
};

//Finding actions done
textForm.addEventListener("submit",e => {
    e.preventDefault();
    speak();
    textInput.blur();
});
//Change of pitch and speed
rate.addEventListener("change", e => (rateValue.textContent = rate.value));
pitch.addEventListener("change", e => (pitchValue.textContent = pitch.value));

voiceSelect.addEventListener("change", e => speak());

# Text to Speech
Ever had troubles getting the pronunciation of a trivial word right. Well [Domics](https://www.youtube.com/watch?v=eNGgfPs0Xp8) sure did. Now you can type the word out and boom, your computer talks out the word perfectly.
<br />
Not an American? Fear not we have over 30 voices including a Japanese accent!

### What it looks like?
![alt text](https://github.com/flowkraD/Text2Speech/blob/master/img/Screenshot%20from%202018-09-07%2023-05-12.png)
Thank you Lord for giving us Bootstrap. Just kidding, thank you Jacob Thornton for Bootstrap.
Thanks to the kind guy who put the GIF displayed as background on imgur. You can find it [here](https://imgur.com/gallery/0Slze).

### Where are the Dependencies?
Glad you asked, this app is free of any external dependencied. That said, you still need a  browser which supports speech synthesis API else you will definetly run into trouble. The latest version of Google Chrome and Mozilla Firefox Dev handles it fine as per the tests. If you have one of those, i guess you are good to go. 

### Contributions
Feedbacks and codebacks are highly recommended (^_^)

### Just run the HTML and you are set
